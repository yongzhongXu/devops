package com.eastcom.devops.devops.controller;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Project Name:devops <br/>
 * Package Name:com.eastcom.devops.devops.controller <br/>
 * Date:2019/6/17 14:07 <br/>
 * <b>Description:</b> TODO: 描述该类的作用 <br/>
 *
 * @author <a href="mailto:xuyz@eastcom-sw.com">Xuyz</a><br/>
 * Copyright Notice =========================================================
 * This file contains proprietary information of Eastcom Technologies Co. Ltd.
 * Copying or reproduction without prior written approval is prohibited.
 * Copyright (c) 2019 =======================================================
 */
@Controller
public class IndexController {
    private  final Logger logger = LoggerFactory.getLogger(this.getClass());
    @RequestMapping("/index")
    public String index(Model model ,HttpServletRequest request){
        String userName = (String)request.getSession().getAttribute("loginUser");
        logger.info("============"+userName);
        model.addAttribute("userName","admin");
        return "index";    /**返回的是显示数据的html的文件名*/
    }

    @RequestMapping("/index2")
    public String index2(Model model ,HttpServletRequest request){
        String userName = (String)request.getSession().getAttribute("loginUser");
        logger.info("============"+userName);
        model.addAttribute("userName","admin");
        return "index2";    /**返回的是显示数据的html的文件名*/
    }
}
