package com.eastcom.devops.devops.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Project Name:devops <br/>
 * Package Name:com.eastcom.devops.devops.controller <br/>
 * Date:2019/6/17 15:52 <br/>
 * <b>Description:</b> TODO: 描述该类的作用 <br/>
 *
 * @author <a href="mailto:xuyz@eastcom-sw.com">Xuyz</a><br/>
 * Copyright Notice =========================================================
 * This file contains proprietary information of Eastcom Technologies Co. Ltd.
 * Copying or reproduction without prior written approval is prohibited.
 * Copyright (c) 2019 =======================================================
 */

@Controller
@RequestMapping("/page")
public class PageCtroller {
    @RequestMapping("/homePage")
    public String homePage(Model model ,HttpServletRequest request){
        return "module/homePage";
    }

    @RequestMapping("/needsPage")
    public String needsPage(Model model ,HttpServletRequest request){
        return "module/needsPage";
    }

    @RequestMapping("/pipelinePage")
    public String pipelinePage(Model model ,HttpServletRequest request){
        return "module/pipelinePage";
    }

    @RequestMapping("/codeManagePage")
    public String codeManagePage(Model model ,HttpServletRequest request){
        return "module/codeManagePage";
    }

    @RequestMapping("/packageManagePage")
    public String packageManagePage(Model model ,HttpServletRequest request){
        return "module/packageManagePage";
    }

    @RequestMapping("/disposePage")
    public String disposePage(Model model ,HttpServletRequest request){
        return "module/disposePage";
    }

    @RequestMapping("/reportPage")
    public String reportPage(Model model ,HttpServletRequest request){
        return "module/reportPage";
    }
}
