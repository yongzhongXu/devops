package com.eastcom.devops.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevopsWebApplication.class, args);
    }

}
