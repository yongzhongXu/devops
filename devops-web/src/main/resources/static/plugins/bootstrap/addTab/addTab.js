var doAddTabs = function (obj) {
    var id = obj.id;
    clearCssOn();
    //如果TAB不存在，创建一个新的TAB
    if (!$("#" + id)[0]) {
    	var titleShow = obj.title;
    	if(titleShow.length > 16) {
    		titleShow = titleShow.substring(0,16)+"...";
    	}
        //创建新TAB的title
        var title = '<li role="presentation" id="tab_' + id + '"><a style="border:0" onclick="javascript:activateTab(\''+id+'\')" href="#' + id + '" aria-controls="' + id + '" role="tab" data-toggle="tab">' + titleShow +'</a>';
        //是否允许关闭
        if (obj.close) {
            title += ' <i class="m_close"></i>';
        }
        title += '</li>';

        //是否指定TAB内容
        if (obj.content) {
            content = '<div role="tabpanel" class="tab-pane" id="' + id + '">' + obj.content + '</div>';
        } else {//没有内容，使用IFRAME打开链接
        	 content = '<div role="tabpanel" class="tab-pane" id="' + id + '"><iframe frameborder="no" id="frm_' + id + '" border="0" onload="javascript:dyniframesize(this.id);" src="' 
        	 	+ obj.url + '" width="100%" scrolling="auto" allowtransparency="yes"></iframe></div>';
            //content = '<div role="tabpanel" class="tab-pane" id="' + id + '"></div>';
        }
        //加入TABS
        $(".nav-tabs").append(title);
        $(".tab-content").append(content);
        //$('#'+id).html('<br> 页面加载中，请稍后...'); // 设置页面加载时的loading图片
        //$('#'+id).load(obj.url); // ajax加载页面
    }
    setCssOn(id);
};

var addTabs = function (id, title, url) {
	doAddTabs({id: id, title: title, url: url, close: true});
}

function dyniframesize(down) {
	/*var ifm = document.getElementById(down); 
	if(ifm) {
	//	var subWeb = window.frames ? window.frames[down].document : ifm.contentDocument; 
//		var subWeb = ifm.contentDocument; 
		var subWeb = document.frames ? document.frames[down].document : ifm.contentDocument; 
		if(subWeb) { 
			ifm.height = subWeb.body.scrollHeight + $("#header").height();
		}
	}*/
	var ifm= document.getElementById(down);   
	var subWeb = document.frames ? document.frames[down].document : ifm.contentDocument;
	var flag = false;
	if(ifm != null && subWeb != null) {
		setInterval(function(){
			var height = subWeb.body.scrollHeight;
			if(ifm.height < height) {
				ifm.height = $("#header").height() + $("#navListId").height() + height ;
			}
		},1000);
	}   
} 

var clearCssOn = function(){
	$(".opened").removeClass("opened");
    $(".active").removeClass("active");
    $(".on").removeClass("on");
};

var setCssOn = function(id){
	$("#tab_" + id).addClass("active on");
    $("#tab_" + id).children("i").addClass("opened");
    $("#" + id).addClass("active");
};

//激活TAB;
var activateTab = function(id){
	clearCssOn();
	setCssOn(id);
};

var closeTab = function (id) {
    //如果关闭的是当前激活的TAB，激活他的前一个TAB
	var pre = $("#tab_" + id).prev();
    if ($("li.active").attr('id') == "tab_" + id) {
    	var pre = $("#tab_" + id).prev();
    	pre.addClass('active').addClass('on');
    	pre.children("i").addClass("opened");
        $("#" + id).prev().addClass('active');
    }
    //关闭TAB
    $("#tab_" + id).remove();
    $("#" + id).remove();
};

$(function () {
    $(".nav-tabs").on("click", ".m_close", function () {
        var id = $(this).prev("a").attr("aria-controls");
        closeTab(id);
    });
});